import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  errorMessage:string;
  signinForm:FormGroup;
  
  constructor(private formbuilder:FormBuilder, private authSerice:AuthService, private router:Router) { }

  ngOnInit() {
    this.initForm();
  }
  initForm(){
    this.signinForm=this.formbuilder.group({
      email:['',[Validators.required, Validators.email]],
      password:['',[Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }
  
  onSubmit(){
    const email=this.signinForm.get('email').value;
    const password=this.signinForm.get('password').value;

    this.authSerice.signInUser(email,password).then(
      ()=>{
        this.router.navigate(['/books']);
      },
      (error)=>{
        this.errorMessage=error;
      }
    );
  }
}
