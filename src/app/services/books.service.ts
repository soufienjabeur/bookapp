import { Injectable } from '@angular/core';
import { Book } from '../Models/Book.model';
import { Subject } from 'rxjs';
import *as firebase from 'firebase'
import DataSnapshot = firebase.database.DataSnapshot;
import { resolve } from 'url';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books:Book[]=[];
  bookSubject =new Subject<Book[]>();

  constructor() { 
    this.getBooks();
  }

  emitBook(){
    this.bookSubject.next(this.books);
  }

  saveBook(){
    firebase.database().ref('/books').set(this.books);
  }
  getBooks(){
    firebase.database().ref('/books').on(
      'value',(data: DataSnapshot)=>{
        this.books=data.val()?data.val():[];
        this.emitBook();
      }
    );
  }

  getSingleBooks(id:number){
    return new Promise(
      (resolve, reject)=>{
        firebase.database().ref('/books/'+id).once('value').then(
          (data:DataSnapshot)=>{
            resolve(data.val());
          },
          (error)=>{
            reject(error);
          }
        );
      }
    );
  }

  creatNewBook(newBook:Book){
    this.books.push(newBook);
    this.saveBook();
    this.emitBook();
  }

  removeBook(book:Book){

    if(book.photo){
      const storageRef=firebase.storage().refFromURL(book.photo);
      storageRef.delete().then(
        ()=>{
          console.log('Photo removed !');
        },
        (error)=>{
          console.log('Could not remove photo! :'+error);
        }
      );
    }
    const bookIndexToRemove=this.books.findIndex(
      (bookEl)=>{
        if(bookEl===book){
          return true;
        }
      }
    );
    this.books.splice(bookIndexToRemove,1);
    this.saveBook();
    this.emitBook();
  }

  uploadFile(file:File){
    return new Promise(
      (resolve, reject)=>{
        const almostUniqueFileName=Date.now().toString();
        const upload=firebase.storage().ref().child('images/'+ almostUniqueFileName+file.name).put(file);
        upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
            ()=>{
              console.log("chargement ...");
            },
            (error)=>{
              console.log("Erreur de chargement !")
              reject();
            },
            ()=>{
              resolve(upload.snapshot.ref.getDownloadURL());
            }
          );
      }
    );
  }
}
