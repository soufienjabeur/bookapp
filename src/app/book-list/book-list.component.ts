import { Component, OnInit, OnDestroy } from '@angular/core';
import { Book } from '../Models/Book.model';
import { Subscription } from 'rxjs';
import { BooksService } from '../services/books.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit, OnDestroy {

  books:Book[];
  booksSubscription:Subscription;

  constructor(private booksServices: BooksService, private router:Router) { }

  ngOnInit() {
    this.booksSubscription=this.booksServices.bookSubject.subscribe(
      (books:Book[])=>{
        this.books=books;
      }
    );
    this.booksServices.emitBook();
  }

  onNewBook(){
    this.router.navigate(['/books','new']);
    }

  onDeleteBook(book:Book){
    this.booksServices.removeBook(book);
  }

  onViewBook(id:Number){
    this.router.navigate(['/books','view',id]);
  }

  ngOnDestroy(){
    this.booksSubscription.unsubscribe();
  }
}
