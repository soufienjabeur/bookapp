import { Component } from '@angular/core';
import *as firebase from 'firebase'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(){
    const firebaseConfig = {
      apiKey: "AIzaSyCR47btoqp9kMFDWMrkIYMWpKJhw7xPrlQ",
      authDomain: "bookapp-5277d.firebaseapp.com",
      databaseURL: "https://bookapp-5277d.firebaseio.com",
      projectId: "bookapp-5277d",
      storageBucket: "bookapp-5277d.appspot.com",
      messagingSenderId: "33202340550",
      appId: "1:33202340550:web:43a7fa5410578ca8"
    };
    firebase.initializeApp(firebaseConfig);
  }
  
}
